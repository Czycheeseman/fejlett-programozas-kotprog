/**
 * @class my_tree
 * @author Andras
 * @date 31/10/2018
 * @file feladat.cpp
 * @brief This class implements a binary tree node 
 */
template<class T>
class my_tree {
private:

   /**
	* @brief The data stored in the node.
	*/
	T _data;
	
   /**
	* @brief The parent of the node.
	*/
	my_tree<T> *_parent;

   /**
	* @brief The left child of the node.
	*/
	my_tree<T> *_leftChild;
	
   /**
	* @brief The right child of the node.
	*/
	my_tree<T> *_rightChild;
	
public:

	/**
	 * @brief Default constructor of the class.
	 */
	my_tree() : _data(NULL), _parent(NULL), _leftChild(NULL), _rightChild(NULL) {}

	/**
	 * @brief Constructor which initialises the data stored in the node.
	 * @param data - The data stored in the node.
	 */
	my_tree(const T &data) : _data(data), _parent(NULL), _leftChild(NULL), _rightChild(NULL) {}

	 /**
	  * @brief Constructor which initialises the data stored in the node, and the left and right children.
	  * @param data - The data stored in the node.
	  * @param left - The left child of the node.
	  * @param right - The right child of the node.
	  */
    my_tree(const T &data, my_tree<T> *left, my_tree<T> *right) : _data(data), _parent(NULL), _leftChild(left), _rightChild(right) {
		if (left != NULL) {
			_leftChild->setParent(this);
		}
		
		if (right != NULL) {
			_rightChild->setParent(this);
		}
	}

	/**
	 * @brief The destructor frees up the whole tree, deletes the _parent, _leftChild and _rightChild member pointers.
	 */
    ~my_tree() {
		delete _leftChild;
		delete _rightChild;
	}

	/**
	 * @brief (const) Getter for the data stored in the node.
	 * @return _data - The data stored in the node.
	 */
    const T& data() const {
		return this->_data;
	}

	/**
	 * @brief Getter for the data stored in the node.
	 * @return _data - The data stored in the node.
	 */
    T& data() {
		return this->_data;
	}

	/**
	 * @brief Getter for the parent of the node
	 * @return _parent - The parent of the node
	 */
    my_tree<T>* parent() const {
		return this->_parent;
	}

	/**
	 * @brief Getter for the left child of the node
	 * @return _leftChild - The left child of the node
	 */
    my_tree<T>* leftChild() const {
		return this->_leftChild;
	}
	
	/**
	 * @brief Getter for the right child of the node
	 * @return _rightChild - The right child of the node
	 */
    my_tree<T>* rightChild() const {
		return this->_rightChild;
	}

	/**
	 * @brief Sets the corresponding child to NULL
	 * @param left - If this is true, then the function sets the left child to NULL; otherwise it sets the right child to NULL
	 */
	void setChildToNULL(bool left) {
		if (left) {
			this->_leftChild = NULL;
		} else {
			this->_rightChild = NULL;
		}
	}

	/**
	 * @brief Setter for the parent of the node
	 * @param parent - The parent of the node
	 */
	void setParent(my_tree<T> *parent) {
		if (this->_parent != NULL) {
			if (this->_parent->isItLeftChild(this)) {
					this->_parent->setChildToNULL(true);
			} else {
					this->_parent->setChildToNULL(false);
			}
		}
		
		this->_parent = parent;
	}
	
	/**
	 * @brief Setter for the left child of the node
	 * @param left - The left child of the node
	 */
    void setLeftChild(my_tree<T> *left) {
		if (this->_leftChild != NULL) {			
			this->_leftChild->setParent(NULL);
		}
		
		this->_leftChild = left;
		
		if (left != NULL) {
			this->_leftChild->setParent(this);
		}
	}
	
	/**
	 * @brief Setter for the right child of the node
	 * @param right - The right child of the node
	 */
    void setRightChild(my_tree<T> *right) {
		if (this->_rightChild != NULL) {
			this->_rightChild->setParent(NULL);
		}
			
		this->_rightChild = right;
			
		if (right != NULL) {
			this->_rightChild->setParent(this);
		}
	}


	/**
	 * @brief Check if the given my_tree<T> parameter is the right child or not
	 * @param child - The child of the node
	 * @return True - If the given child is the left child, False - Otherwise
	 */
	bool isItLeftChild(my_tree<T> *child) {
		return this->_leftChild == child ? true : false;
	}

	/**
	 * @class iterator
	 * @author Torok Andras
	 * @date 21/10/2018
	 * @file feladat.cpp
	 * @brief Inner class which implements the iterator what does the inorder process
	 */
	class iterator {
	public:
		/** The constructor of the iterator for *node */
		iterator(my_tree<T> *node) : current(node) {}
		
		/** Overriding the ++ prefix operator of the Iterator */
		iterator &operator++() {
			nextInorder(current);
			
			return *this;
		}
		
		/** Overriding the ++ postfix operator of the Iterator */
		iterator operator++(int) {
			my_tree<T> *temp = current;
			
			nextInorder(current);
			
			return temp;
		}
		
		/** Overriding the == operator of the Iterator */
		bool operator==(const iterator &other) const {
			return current == other.current;
		}
		
		/** Overriding the != operator of the Iterator */
		bool operator!=(const iterator &other) const {
			return current != other.current;
		}
		
		/** Overriding the * operator of the Iterator to access data */
		T& operator*() const {
			return (*current).data();
		}
		
		/** Overriding the -> operator of the Iterator to access data */
		T* operator->() const {
			return &((*current).data());
		}
		
	private:
		/** The current node referenced by the iterator */
		my_tree<T> *current;
		
		/**
		 * @brief Sets the current node to the next one according to the inorder process
		 * @param node
		 */
		void nextInorder(my_tree<T> *node) {
			if (node != NULL) {
				nextInorder(node->leftChild());
				current = node;
				nextInorder(node->rightChild());
			}
		}
	};
	
	/**
	 * @brief Iterator for the first element in the tree according to the inorder process
	 * @return 
	 */
	iterator begin() {
		my_tree<T>* current = this;
		
		while(current->leftChild() != NULL) {
			current = current->leftChild();
		}
		
		return iterator(current);
	}
	
	/**
	 * @brief Iterator for the element after the last element in the tree according to the inorder process
	 * @return 
	 */
	iterator end() {
		my_tree<T>* current = this;
		
		while(current->rightChild() != NULL) {
			current = current->rightChild();
		}
		
		return iterator(current);
	}
};