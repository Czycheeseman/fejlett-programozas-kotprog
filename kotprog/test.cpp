/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
READ AGAIN THE DESCRIPTION OF THE EXERCISE
SOME STUFF HAVE TO BE REMOVED FROM THIS FILE
BEFORE SUBMITING IT TO THE BIRO SYSTEM
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

#include <iostream>
#include "tree_base.h"

using namespace std;

/**
 * @class my_tree
 * @author Andras
 * @date 24/10/2018
 * @file test.cpp
 * @brief This class implements a binary tree node 
 */
template<class T>
class my_tree : public tree_base {
private:

   /**
	* @brief The data stored in the node.
	*/
	T _data;
	
   /**
	* @brief The parent of the node.
	*/
	my_tree<T> *_parent;

   /**
	* @brief The left child of the node.
	*/
	my_tree<T> *_leftChild;
	
   /**
	* @brief The right child of the node.
	*/
	my_tree<T> *_rightChild;
	
public:
/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------CONSTRUCTORS AND DESTRUCTOR-------------------------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |------------------------------------------------------------BEGIN----------------------------------------------------------------------|	
 */

	/**
	 * @brief Default constructor of the class.
	 */
	my_tree() : _data(NULL), _parent(NULL), _leftChild(NULL), _rightChild(NULL) {}

	/**
	 * @brief Constructor which initialises the data stored in the node.
	 * @param data - The data stored in the node.
	 */
	my_tree(const T &data) : _data(data), _parent(NULL), _leftChild(NULL), _rightChild(NULL) {}

	 /**
	  * @brief Constructor which initialises the data stored in the node, and the left and right children.
	  * @param data - The data stored in the node.
	  * @param left - The left child of the node.
	  * @param right - The right child of the node.
	  */
    my_tree(const T &data, my_tree<T> *left, my_tree<T> *right) : _data(data), _parent(NULL), _leftChild(left), _rightChild(right) {
		if (left != NULL) {
			_leftChild->setParent(this);
		}
		
		if (right != NULL) {
			_rightChild->setParent(this);
		}
	}

	/**
	 * @brief The destructor frees up the whole tree, deletes the _parent, _leftChild and _rightChild member pointers.
	 */
    ~my_tree() {
		delete _leftChild;
		delete _rightChild;
	}
	
	
/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------CONSTRUCTORS AND DESTRUCTOR-------------------------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |--------------------------------------------------------------END----------------------------------------------------------------------|	 
 */
	
/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------GETTERS AND SETTERS---------------------------------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |------------------------------------------------------------BEGIN----------------------------------------------------------------------| 
 */

/**
 * |------------------------------------------------------------GETTERS--------------------------------------------------------------------|
 */
	
	/**
	 * @brief (const) Getter for the data stored in the node.
	 * @return _data - The data stored in the node.
	 */
    const T& data() const {
		return this->_data;
	}

	/**
	 * @brief Getter for the data stored in the node.
	 * @return _data - The data stored in the node.
	 */
    T& data() {
		return this->_data;
	}
	
	/**
	 * @brief Getter for the parent of the node
	 * @return _parent - The parent of the node
	 */
    my_tree<T>* parent() const {
		return this->_parent;
	}

	/**
	 * @brief Getter for the left child of the node
	 * @return _leftChild - The left child of the node
	 */
    my_tree<T>* leftChild() const {
		return this->_leftChild;
	}
	
	/**
	 * @brief Getter for the right child of the node
	 * @return _rightChild - The right child of the node
	 */
    my_tree<T>* rightChild() const {
		return this->_rightChild;
	}

/**
 * |------------------------------------------------------------SETTERS---------------------------------------------------------------------|
 */
	
	/**
	 * @brief Sets the corresponding child to NULL
	 * @param left - If this is true, then the function sets the left child to NULL; otherwise it sets the right child to NULL
	 */
	void setChildToNULL(bool left) {
		if (left) {
			this->_leftChild = NULL;
		} else {
			this->_rightChild = NULL;
		}
	}
	
	/**
	 * @brief Setter for the parent of the node
	 * @param parent - The parent of the node
	 */
	void setParent(my_tree<T> *parent) {	
		if (this->_parent != NULL) {
			if (this->_parent->isItLeftChild(this)) {
					this->_parent->setChildToNULL(true);
			} else {
					this->_parent->setChildToNULL(false);
			}
		}
		
		this->_parent = parent;
	}
	
	/**
	 * @brief Setter for the left child of the node
	 * @param left - The left child of the node
	 */
		void setLeftChild(my_tree<T> *left) {
			if (this->_leftChild != NULL) {			
				this->_leftChild->setParent(NULL);
			}
			
			this->_leftChild = left;
		
			if (left != NULL) {
				this->_leftChild->setParent(this);
			}
		}
	
	/**
	 * @brief Setter for the right child of the node
	 * @param right - The right child of the node
	 */
    void setRightChild(my_tree<T> *right) {
		if (this->_rightChild != NULL) {
			this->_rightChild->setParent(NULL);
		}
			
		this->_rightChild = right;
			
		if (right != NULL) {
			this->_rightChild->setParent(this);
		}
	}

/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------GETTERS AND SETTERS---------------------------------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |------------------------------------------------------------END------------------------------------------------------------------------| 
 */

/**
 * @brief Check if the given my_tree<T> parameter is the right child or not
 * @param child - The child of the node
 * @return True - If the given child is the left child, False - Otherwise
 */
bool isItLeftChild(my_tree<T> *child) {
	return this->_leftChild == child ? true : false;
}

/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------ITERATOR TYPE, INNER CLASS AND OPERATIONS-----------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |------------------------------------------------------------BEGIN----------------------------------------------------------------------| 
 */

/**
 * |------------------------------------------------------------ITERATOR INNER CLASS-------------------------------------------------------| 
 */

	/**
	 * @class iterator
	 * @author Torok Andras
	 * @date 21/10/2018
	 * @file test.cpp
	 * @brief Inner class which implements the iterator what does the inorder process
	 */
	class iterator {
	public:
		/** The constructor of the iterator for *node */
		iterator(my_tree<T> *node) : current(node) {}
		
		/** Overriding the ++ prefix operator of the Iterator */
		iterator &operator++() {
			nextInorder(current);
			
			return *this;
		}
		
		/** Overriding the ++ postfix operator of the Iterator */
		iterator operator++(int) {
			my_tree<T> *temp = current;
			
			nextInorder(current);
			
			return temp;
		}
		
		/** Overriding the == operator of the Iterator */
		bool operator==(const iterator &other) const {
			return current == other.current;
		}
		
		/** Overriding the != operator of the Iterator */
		bool operator!=(const iterator &other) const {
			return current != other.current;
		}
		
		/** Overriding the * operator of the Iterator to access data */
		T& operator*() const {
			return (*current).data();
		}
		
		/** Overriding the -> operator of the Iterator to access data */
		T* operator->() const {
			return &((*current).data());
		}
		
		private:
			/** The current node referenced by the iterator */
			my_tree<T> *current;
			
			/**
			 * @brief Sets the current node to the next one according to the inorder process
			 * @param node
			 */
			void nextInorder(my_tree<T> *node) {
				if (node != NULL) {
					nextInorder(node->leftChild());
					current = node;
					nextInorder(node->rightChild());
				}
			}
	};

/**
 * |------------------------------------------------------------ITERATOR TYPE--------------------------------------------------------------| 
 */

	// Iterator type which covers the inorder process in the tree
	/*class iterator {
		
	};*/

/**
 * |------------------------------------------------------------OPERATIONS-----------------------------------------------------------------| 
 */
	
	/**
	 * @brief Iterator for the first element in the tree according to the inorder process
	 * @return 
	 */
	iterator begin() {
		cout << "begin(): START at: " << this << endl;
		my_tree<T>* current = this;
		
		while(current->leftChild() != NULL) {
			current = current->leftChild();
		}
		
		return iterator(current);
		
		cout << "begin(): END at: " << this << endl;
	}
	
	/**
	 * @brief Iterator for the element after the last element in the tree according to the inorder process
	 * @return 
	 */
	iterator end() {
		cout << "end(): START at: " << this << endl;
		my_tree<T>* current = this;
		
		while(current->rightChild() != NULL) {
			current = current->rightChild();
		}
		
		return iterator(current);
		cout << "end(): END at: " << this << endl;
	}
	
/**
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |-----------------------------------------------------ITERATOR TYPE AND OPERATIONS------------------------------------------------------|
 * |---------------------------------------------------------------------------------------------------------------------------------------|
 * |--------------------------------------------------------------END----------------------------------------------------------------------| 
 */

/**
 * @brief To print out all of the member data found in the node.
 */
void print() {
	cout << endl;
	cout << "Node: " << this << ", Data: " << this->data() << ", Parent: " << this->parent() << ", Left child: " << this->leftChild() << ", Right child: " << this->rightChild() << endl;
	cout << endl;
}

/**
 * @brief To recursively print out all of the member data found in the node and in all of its children.
 */
void print(my_tree<T> *node) {
	if (node != NULL) {
		cout << endl;
		cout << "Node: " << this << ", Data: " << this->data() << ", Parent: " << this->parent() << ", Left child: " << this->leftChild() << ", Right child: " << this->rightChild() << endl;
		cout << endl;
		
		node->leftChild()->print(node->leftChild());
		node->rightChild()->print(node->rightChild());
	}
}

};

int main()
{	
	//my_tree<int> *tree = new my_tree<int>(8, new my_tree<int>(6, new my_tree<int>(5), new my_tree<int>(7)), new my_tree<int>(10, new my_tree<int>(9), new my_tree<int>(11)));
	/*my_tree<int> *left = new my_tree<int>(7);
	my_tree<int> *right = new my_tree<int>(9);
	my_tree<int> *tree = new my_tree<int>(8, left, right);
	
	tree->print(tree);

	delete tree;

	tree->print(tree);*/
	
	// Tesztesetek
	// 1. teszteset - Hiba: nem feladatkiírás szerinti működés (Próba 4 után)
	/*my_tree<int> *tree = new my_tree<int>(1, new my_tree<int>(2), new my_tree<int>(3));
	
	tree->print(tree);
	
	cout << tree->data() << " " << tree->leftChild()->data() << " " << tree->rightChild()->data() << endl;
	
	delete tree;
	
	tree->print(tree);*/
	
	// 2. teszteset - Hiba: nem feladatkiírás szerinti működés (Próba 4 után)
	/*my_tree<int> *tree = new my_tree<int>(1);
	
	tree->print(tree);
	
	tree->data() = 1000;
	
	tree->print(tree);
	
	delete tree;
	
	tree->print(tree);*/
	
	// 3. teszteset - Hiba: nem feladatkiírás szerinti működés (Próba 4 után)
	/*my_tree<int> *tree1 = new my_tree<int>(1);
	my_tree<int> *tree2 = new my_tree<int>(2);
	my_tree<int> *tree3 = new my_tree<int>(3);
	
	cout << "tree1" << endl;
	tree1->print();
	
	cout << "tree2" << endl;
	tree2->print();
	
	cout << "tree3" << endl;
	tree3->print();
	
	tree1->setLeftChild(tree2);
	tree1->setRightChild(tree3);
	
	cout << "After set!" << endl;
 	
	tree1->print(tree1);
	
	delete tree1;*/
	
	// 4. teszteset - Hiba: nem feladatkiírás szerinti működés (Próba 4 után)
	/*my_tree<int> *tree1 = new my_tree<int>(1, new my_tree<int>(2, new my_tree<int>(4), new my_tree<int>(5)), new my_tree<int>(3, new my_tree<int>(6), new my_tree<int>(7)));
	my_tree<int> *tree2 = tree1->leftChild();
	my_tree<int> *tree3 = tree2->leftChild();
	my_tree<int> *tree4 = tree1->rightChild();
	my_tree<int> *tree5 = tree4->rightChild();
	
	cout << "tree1" << endl;
	tree1->print(tree1);
	
	cout << "tree2" << endl;
	tree2->print(tree2);
	
	cout << "tree3" << endl;
	tree3->print(tree3);
	
	cout << "tree4" << endl;
	tree4->print(tree4);
	
	cout << "tree5" << endl;
	tree5->print(tree5);
	
	tree2->setLeftChild(tree5);

	cout << "After set: tree2->setLeftChild(tree5);" << endl;
	
	cout << "tree2" << endl;
	tree2->print(tree2);
	
	cout << "tree3" << endl;
	tree3->print(tree3);
	
	cout << "tree4" << endl;
	tree4->print(tree4);
	
	cout << "tree5" << endl;
	tree5->print(tree5);

	cout << tree3->parent() << " " << tree4->rightChild() << endl;
	
	delete tree1;
	delete tree3;*/
	
	// 5. teszteset - Hiba: nem feladatkiírás szerinti működés (Próba 4 után)
	/*my_tree<int> *tree1 = new my_tree<int>(1, new my_tree<int>(2, new my_tree<int>(4), new my_tree<int>(5)), new my_tree<int>(3, new my_tree<int>(6), new my_tree<int>(7)));
	my_tree<int> *tree2 = tree1->leftChild();
	my_tree<int> *tree3 = tree2->leftChild();
	my_tree<int> *tree4 = tree1->rightChild();
	my_tree<int> *tree5 = tree4->rightChild();
	
	cout << "tree1" << endl;
	tree1->print(tree1);
	
	cout << "tree2" << endl;
	tree2->print(tree2);
	
	cout << "tree3" << endl;
	tree3->print(tree3);
	
	cout << "tree4" << endl;
	tree4->print(tree4);
	
	cout << "tree5" << endl;
	tree5->print(tree5);
	
	cout << "tree4->setRightChild(tree3)" << endl;
	
	tree4->setRightChild(tree3);
	
	cout << "tree2" << endl;
	tree2->print(tree2);
	
	cout << "tree3" << endl;
	tree3->print(tree3);
	
	cout << "tree4" << endl;
	tree4->print(tree4);
	
	cout << "tree5" << endl;
	tree5->print(tree5);
	
	cout << "tree5->parent(): " << tree5->parent() << " " << "tree2->leftChild(): " << tree2->leftChild() << endl;
	
	delete tree1;
	delete tree5;*/
	
	// 6. teszteset - Hiba: futási hiba: 25 (Próba 4 után)
	/*my_tree<int> *tree = new my_tree<int>(1, new my_tree<int>(2), new my_tree<int>(3));
	
	for (my_tree<int>::iterator it = tree->begin(); it != tree->end(); ++it) {}
		
	delete tree;*/
	
	// 7. teszteset - Hiba: futási hiba: 25 (Próba 4 után)
	// Itt vmi tester-es cucc van
	
	// 8. teszteset - Hiba: futási hiba: 25 (Próba 4 után)
	/*my_tree<int> *tree1 = new my_tree<int>(1, new my_tree<int>(2), new my_tree<int>(3));
	
	my_tree<int> *tree2 = tree1->leftChild()->leftChild();
	
	for (my_tree<int>::iterator it = tree2->begin(); it != tree2->end(); ++it) {}
	
	delete tree1;*/
	
	return 0;
}