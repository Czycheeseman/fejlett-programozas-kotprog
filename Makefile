.PHONY: clean All

All:
	@echo "----------Building project:[ kotprog - Debug ]----------"
	@cd "kotprog" && "$(MAKE)" -f  "kotprog.mk"
clean:
	@echo "----------Cleaning project:[ kotprog - Debug ]----------"
	@cd "kotprog" && "$(MAKE)" -f  "kotprog.mk" clean
